import sys
from sys import argv
import nltk
from nltk import FreqDist, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import csv
import re

wordcount = {} #dictionary to associate word with count

def takeLowestCount(elem):
	return elem[1].get('count');

stop_words = set(stopwords.words('english'))
stop_words.add(('http','RT', 'a', '\'re'))

lemmatizer = WordNetLemmatizer()
#for each file passed in
for i in range(1, len(argv)):

	text = open(argv[i], 'r', encoding='utf-8')

	csvv = csv.reader(text)
	#get rid of user info and time
	purely_tweets = list(map(lambda row: row[2], csvv))
	for tweet in purely_tweets:
		#first remove all links becuase http being included in wordcounts

		toks = nltk.tokenize.word_tokenize(tweet)
		#remove small words and punctuation
		clean_tokens = []
		for w in toks:
			if w not in stop_words:
				if re.sub(r'[^\w\s]','',w) is not "":
					clean_tokens.append(w)

		#add part of speech to each token
		tokens_pos = nltk.pos_tag(clean_tokens)
		#only keep noun verb adj
		is_NounOrAdjOrVerb = lambda pos: pos[:2] == 'NN' or pos[:2] == 'JJ' or pos[:2] == 'VB'
		noun_verb_adj_tokens = [(word, pos) for (word, pos) in tokens_pos if is_NounOrAdjOrVerb(pos)]
		#for each token lemmatize and count
		for tok, pos in noun_verb_adj_tokens:
			lemma = lemmatizer.lemmatize(tok)
			if lemma.lower() in wordcount:
				count = wordcount[lemma.lower()].get('count')+ 1
				wordcount[lemma.lower()].update({'count': count})
			else:
				wordcount[lemma.lower()]={'count': 1}


#sort so highest counts an front of list
wordcount = sorted(wordcount.items(), key=takeLowestCount, reverse=True)
for word, value in wordcount[1:30]:
	print(word +"\tcount= "+ str(value['count']))
